<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Cache-Control" content="max-age=86400, must-revalidate" />
        <title>
        
            The two commandments for undergrad math / TCS courses - Linus Heck
        
        </title>
        <link href="https://fonts.googleapis.com/css2?family=Alegreya&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" title="hakyll_theme" href="../css/default.css" />
        <link rel="stylesheet" href="../css/syntax.css" />
    </head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-RM8TG9RCN6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-RM8TG9RCN6');
    </script>
    <body>
      <header>
        <h2><a href="../">Linus Heck</a></h2>
        <nav>
          <a href="../">Projects</a>
          <a href="../blog.html">Blog</a>
        </nav>
      </header>
        <div class="main-container">
        <div id="content" class="inside">
            <article>
    <section class="header">
        Posted on July 13, 2020
        
    </section>
    <h2>The two commandments for undergrad math / TCS courses</h2>
    <section>
        <p>I’ve only been tutoring in theoretical computer science undergrad courses for about three semesters now
(and of course taken a fair share of them) but I’ve thought quite a bit about
how to do it <em>right</em>. Of course, there’s a lot of subjective opinion in that:
how do you explain something, and is <span class="math inline">0 ∈ ℕ</span>?
But I think there are some objective things you should
look out for. These (in my opinion) are the two commandments for undergrad math courses:</p>
<ol type="1">
<li>Be kind.
<ul>
<li>Have the right attitude towards your students.</li>
<li>Reduce stress on your students. Value your student’s time.</li>
<li>Consider that your subject is hard.</li>
</ul></li>
<li>Be predictable.
<ul>
<li>Connect your lecture to your assignments and your assignments to your exam.
If you are a bigger team, coordinate these clearly.</li>
<li>Keep a consistent level of exactness.</li>
</ul></li>
</ol>
<p>Fortunately, a lot of people do these right. I’ve had great courses
that were kind and predictable. But important details tend to go wrong
in some courses in my experience.</p>
<h3 id="be-kind">1. Be kind</h3>
<h4 id="have-the-right-attitude-towards-your-students-reduce-stress">Have the right attitude towards your students, reduce stress</h4>
<p>I noticed that some people running undergrad courses have the general (maybe implicit) attitude
that students are lazy, stupid, and always tempted to take the shortcut, even if it
involves cheating or breaking other rules. Not only is this often taken as
explanation that the course is not rated well by the students or the exam has a
70% fail rate, but it’s also often taken into a <em>predisposition</em> towards the students.
This results in paranoid measures like forcing assignment groups of three students
to return their assignments in three different handwritings (to prove that everyone
was working?), letting students stand up and turn around while handing out exam sheets,
or having a general (and often very explicit) “if you spot a mistake, it’s probably your fault”
attitude in exam reviews. If you have taken maths undergrad courses you can probably name
another example off the top of your head.
Some of the measures that I’ve seen were not only completely unnessecary, but actively harmful:
Being paranoid about cheating <em>encourages</em> cheating (and no “anti-cheating” measure is very
hard to get around).
Also, a general level of distrust against students is really harmful for their mental health and level of
confidence in continuing to study.</p>
<p>So: trust that your students don’t take shortcuts. You can’t prevent it anyways,
and if they do, it’s going to be a learning experience for them later on.
And try to make your students understand that they can understand the subject, even if it’s hard.
Of course, that is a hard thing to actually go and do, but it’s not impossible.</p>
<h4 id="value-your-students-time-consider-that-your-subject-is-hard">Value your student’s time, consider that your subject is hard</h4>
<p>Even if you trust your students being intelligent and hard-working, consider how many hours students spend
on the course. Having a hard exercise sometimes is a good learning factor, of course. A math course is always
a test in persistence and motivation. But also consider that the students have different things to do,
and they also need some work-life-balance. Finding the sweet spot here is crucial. Additionally, professors
and assistants tend to forget that their subjects are actually really hard, especially in mathematics,
where basic topics are so internalized that it might seem wild that someone wouldn’t understand
what a homomorphism does. This feeds into the topic of how to <em>really</em> explain something in the lecture,
but it also has to do with how hard you can reasonably make the exercises and exams.</p>
<h3 id="be-predictable">2. Be predictable</h3>
<h4 id="connect-your-lecture-to-your-assignments">Connect your lecture to your assignments…</h4>
<p>This seems obvious, but is often not the case, especially when different people are working on the assignments, the
lecture and the exam. This starts at the lecture, assignments and exam using different notation respectively,
but you should also ask yourself if the assignments can be understood by consulting the lecture. Also, having tutorials
where students can ask questions about exercises is helpful here.</p>
<h4 id="and-your-assignments-to-your-exam">…and your assignments to your exam</h4>
<p>Students should be able to pass the exam by having done the exercises. They should also get a glimpse into what the
exam will look like, for instance by a test exam. Of course, the whole point of an exam is that it’s unpredictable
what will be asked of the students, but it should rather be too predictable than not predictable enough. An exam
is a stress situation for most students, and you shouldn’t ask them to find all-new solutions to all-new problems.</p>
<h4 id="keep-a-consistent-level-of-exactness">Keep a consistent level of exactness</h4>
<p>This is more specific to math courses, I think, than the other points. The students should know what is asked of them
when an exercise is to prove or explain something. If you only do “this is trivial” and “you can see this by looking very
hard”-ish proofs in your lectures, don’t ask your students to prove something by induction on three pages in the exam.
It’s also not an argument that students <em>should have learned to prove something</em> in another lecture, if they haven’t seen
any proofs in yours, they will just mimic what the lecture does, as is sensible of any human being.
Make it clear in the exercises and exam how exact you want the explanation and proof to be.</p>
<h3 id="in-conclusion">In conclusion</h3>
<p>There are probably many more things that need to be said about undergrad courses. This is just what I had on my mind in the
last semester.
Thanks for reading!</p>
    </section>
</article>

        </div>
        </div>
    </body>
</html>
