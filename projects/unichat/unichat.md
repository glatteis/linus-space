---
name: Unichat
coolness: 0
image: /projects/unichat/unichat.png
abstract: An open source, room-based, time-tabled chat app for students of the RWTH Aachen (no one uses it, obviously).
---

[Here](https://unichat.github.io/)'s the link to an instance uploaded on Heroku, and the source code
is available [here (frontend)](https://github.com/glatteis/unichat-js)
and [here (backend)](https://github.com/glatteis/rwth-unichat).
