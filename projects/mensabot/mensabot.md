---
title: RWTH Mensabot
abstract:  A small telegram bot I wrote in python to retrieve information about today's meals. Contrary to all other telegram mensa bots which support Aachen, this bot can show if a meal is vegan, vegetarian, etc, and can filter to these categories. Retrieves data using the RWTH API. 
image: /projects/mensabot/mensabot.png
coolness: 11
---

You don't want to look at the code, but you can find it if you want to. You can talk to the bot now, just click [here](https://t.me/rwthfressbot).
