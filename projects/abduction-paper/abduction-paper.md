---
title: Abduction Paper
image: /projects/abduction-paper/kant.png
abstract:  A philosophy course paper I wrote about abduction and maths (involving Groups, in German).
coolness: 12
---

[There you go.](/projects/abduction-paper/abduction_paper.pdf)
