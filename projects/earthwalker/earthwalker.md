---
title: Earthwalker
image: /projects/earthwalker/earthwalker.jpg
abstract: Earthwalker is a game of a similar concept to GeoGuessr. Host it yourself, no API keys needed.
coolness: 25
---

Earthwalker is a game of a similar concept to [GeoGuessr](https://geoguessr.com).
You get dropped somewhere in the world in Google Street View, and the goal is that you find out where you are,
and guess better than all of your friends. You can play against time, restrict the game to an area, and more.

It's free and open source, and the idea is that people host it themselves to play with their friends. No Google
API keys are needed, as Earthwalker "fools" the public Google Street View a bit. This is technically against Google TOS,
so I am not hosting a public version of this myself.

For more information and to play it, please visit the [Repository](https://gitlab.com/glatteis/earthwalker).
