---
title: A communal bikesharing system for under 10€ per bike
---

![](/images/esel.png)

[We've created a new bicycle sharing system.](https://esel.ac) *It's super cheap:* the electronics on each bicycle only cost 10€, and there are no running costs except the hosting of the website and spare parts for the bicycles. *It's low-tech:* bikes unlock with combination locks instead of expensive automatic locks. For us, it's just a communal project, so it's free for everyone involved. But we also want to share the system's concept, empowering people to make their own sharing systems that are just as inexpensive.

Why a communal bike sharing system? Bicycles are the optimal mode of transport for urban areas, as they are the fastest way to get from place to place and are as sustainable as walking. The issue with owning your own bike is that you have to carry it with you everywhere you go: if you arrive by bike, you have to return by bike. Sharing systems have become a prevalent mode of transport in the last few years, but they are often expensive and, in the case of e-scooters, unsustainable.

You can get all the pros without any cons by sharing a couple of bicycles with your friends or a bigger group of people you can trust. You can hop on a bike close to you when you feel like it, but it's free, unlike renting sharing vehicles. Functional bicycles are abundant. Most people have several in their houses or garages. But for such an arrangement, you need a way to keep track of the bikes that everyone can set up.

Good news: This is possible! We created a working communal bike sharing system in Aachen that costs less than 10€ per bike. You can check it out under [esel.ac](https://esel.ac). We accomplished this by pairing open-source bike sharing software with cheap trackers that emulate Apple AirTags.

To the best of our knowledge, this is *by far* the cheapest bike sharing system. Without the AirTag tracking solution, it costs much more per bike to track the locations: You need a SIM card on each bicycle, and you need a way to power GPS and cell transceivers. We use Bluetooth beacons that run for three years on one battery. It is not currently easy to set this up yourself today, but turning this working system into software that everyone can use to create their own sharing systems is a long-term goal. Experienced people can already set up our sharing systems [using our repositories](https://gitlab.com/eselac). We would be very happy about people who would want to try this out, please [email me](mailto:linus.heck@rwth-aachen.de) so I can send you some more info.
