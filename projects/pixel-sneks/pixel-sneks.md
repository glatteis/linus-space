---
title: PixelSneks
image: /projects/pixel-sneks/pixelsneks.png
abstract: A CurveFever demake, complete with powerups, local mutiplayer, and bots. Made with Lua and the pico8 fantasy console. 
coolness: 4
---

<iframe src="https://www.lexaloffle.com/bbs/widget.php?pid=34769" allowfullscreen width="621" height="513" style="border:none; overflow:hidden"></iframe>

You can find the [game project here](https://www.lexaloffle.com/bbs/?tid=28413).
