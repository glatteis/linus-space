---
title: CarParty
image: /projects/carparty/carparty.jpg
abstract: CarParty is a unique game I built with a team of students at RWTH.
coolness: 23
---

CarParty is a unique multiplayer racing game that uses the phone as input, for drawing of your track
and then for racing. You can play it right now at [car-party.de](https://car-party.de).
We came up with, designed and built this game in a university practical.

You can also [the GitHub repository](https://github.com/CarParty/CarParty).