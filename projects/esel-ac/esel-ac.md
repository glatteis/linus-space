---
title: esel.ac
image: /projects/esel-ac/esel-ac.jpg
abstract: free, communal bikesharing in Aachen
coolness: 30
---

<a href="https://esel.ac">esel.ac</a> is free, communal bikesharing in Aachen. Use it by simply
registering, finding a bike, typing in its number and unlocking it with the code. You could use it
as a free and zero-carbon alternative to e-scooter systems, or "rent" bikes for longer amounts of
time. The system already works, but thanks to a lot of help from friends we have many more bikes
upcoming.

Software-wise, we're really thankful for the open-source projects
<a href="https://github.com/transportkollektiv/openbike">OpenBike</a> and
<a href="https://github.com/seemoo-lab/openhaystack">OpenHaystack</a>. In the future, we plan to
give back by publishing our glue code between the projects.