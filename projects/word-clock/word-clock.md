---
title: Word Clock
image: /projects/word-clock/wordclock.jpg
abstract: A word clock build completely from scratch.
coolness: 15
---

Sorry, there's no real documentation. [This](https://gitlab.com/glatteis/wordclock-backend)
is the backend and you can find the old frontend stuff [here](https://github.com/glatteis/arduino-word-clock),
but it's still missing the new frontend.
