---
title: Pentago
abstract: A sleek game for Android with AI and WiFi network play.
image: /projects/pentago/pentago.png
coolness: 5
---

[Here's the repo.](https://github.com/glatteis/pentago/)
