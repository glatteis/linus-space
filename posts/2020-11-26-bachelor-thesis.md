---
title: Bachelor Thesis
---

My bachelor thesis "Gradient Descent on Parametric Markov Chains" is now uploaded for open
access on the RWTH Aachen publication server. [Here's the link!](https://publications.rwth-aachen.de/record/804571)
