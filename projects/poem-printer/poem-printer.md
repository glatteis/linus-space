---
name: Poem Printer
image: /projects/poem-printer/printer.jpg
abstract: You press the button, you get a poem. It also prints the weather forecast!
coolness: 18
---

You press the button, you get a poem. It also prints the weather forecast!
Look at the [Repo](https://github.com/glatteis/poem-printer/) for a lot more info.
