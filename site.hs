{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
import          Data.Monoid (mappend)
import          Hakyll
import          Text.Pandoc.Options
import          Data.List ( sortBy, sortOn, sortBy )
import          Control.Monad (void, (>>=), liftM)
import          Data.Ord (comparing)
import          Data.Maybe(fromMaybe)
import          Text.Read(readMaybe)
import          Data.Foldable (toList)

main :: IO ()
main = hakyll $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match (fromList ["projects.md"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match "projects/*/*.md" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/project.html" postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match ("projects/*/*.png" .||. "projects/*/*.jpg" .||. "projects/*/*.pdf") $ do
        route idRoute
        compile copyFileCompiler

    create ["blog.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let blogCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    defaultContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/blog.html" blogCtx
                >>= loadAndApplyTemplate "templates/default.html" blogCtx
                >>= relativizeUrls

    create ["index.html"] $ do
        route idRoute
        compile $ do
            projects <- reverse <$> (byCoolness =<< loadAll "projects/*/*.md")
            let blogCtx =
                    listField "projects" postCtx (return projects) `mappend`
                    constField "title" "Projects"                  `mappend`
                    defaultContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/projects.html" blogCtx
                >>= loadAndApplyTemplate "templates/default.html" blogCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

-- this parses the coolness out of an item
-- it defaults to 0 if it's missing, or can't be parsed as an Int
coolness :: MonadMetadata m => Item a -> m Int
coolness i = do
    mStr <- getMetadataField (itemIdentifier i) "coolness"
    return $ fromMaybe 0 (mStr >>= readMaybe)

byCoolness :: MonadMetadata m => [Item a] -> m [Item a]
byCoolness = sortByM coolness
  where
    sortByM :: (Monad m, Ord k) => (a -> m k) -> [a] -> m [a]
    sortByM f xs = map fst . sortBy (comparing snd) <$>
                   mapM (\x -> fmap (x,) (f x)) xs
