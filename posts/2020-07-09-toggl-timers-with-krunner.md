---
title: Starting toggl timers quickly on Linux with KRunner
---

For a boring workflow post today, maybe this helps you.
I can start and stop my toggl timers just by pressing `Alt+Space` and typing the name of the
timer. Cool, right? If you have KRunner, you can also do that! Just install [`krunner`](https://docs.kde.org/trunk5/en/kde-workspace/plasma-desktop/krunner.html)
(if you use KDE, it's preinstalled), and its plugin [`krunner-symbols`](https://github.com/domschrei/krunner-symbols), then 
install [`toggl-cli`](https://github.com/AuHau/toggl-cli), and put this into your `~/.config/krunner-symbolsrc`:
``` toml
[Definitions]
# Assume you have the project "Thesis", then you can add something like this
thesis=exec:/home/<user>/.local/bin/toggl start -o Thesis
# Or for the project "Configuring", something like this:
configuring=exec:/home/<user>/.local/bin/toggl start -o Configuring
# ...

# And of course, something ot stop the running timer.
stop=exec:/home/<user>/.local/bin/toggl stop
```
Replace `<user>` with your user, of course.
You might want to configure `krunner` to start on a certain shortcut (like `Alt+Space` for me).
`toggl` might have a different path for you, try `which toggl`.
