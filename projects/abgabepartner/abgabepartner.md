---
title: abgabepartner.de
image: /projects/abgabepartner/apb.png
abstract: Finding people to work with or swapping tutoriums in new subjects has always been hard. I try to fix this with this website, with serves exactly this purpose. 
coolness: 19
---

Finding people to work with or swapping tutoriums in new subjects has always been hard. I try to fix this with this website, with serves exactly this purpose. 

The project currently runs on [abgabepartner.de](https://abgabepartner.de), unsurprisingly.
For more information, please visit the the [Repository](https://git.rwth-aachen.de/h/apb).
