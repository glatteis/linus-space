---
title: Development Cooperation Lecture at RWTH
image: /projects/development-cooperation/leonardo.png
abstract: "In the summer semester 2021, I was involved in organizing a lecture about development cooperation at the RWTH."
coolness: 20
---

This lecture was organized by Studieren Ohne Grenzen Aachen with the Project Leonardo, and focuses
on topics of (De)Colonialization, Postcolonialism and questions about development cooperation.

[Lecture page on the Project Leonardo website](https://www.leonardo.rwth-aachen.de/de/module/sose-2021/entwicklungszusammenarbeit/)
