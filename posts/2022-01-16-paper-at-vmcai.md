---
title: Paper at VMCAI 2022
---

The paper "Gradient-Descent for Randomized Controllers Under Partial Observability" has been
published at VMCAI 2022: [Link](https://link.springer.com/book/10.1007/978-3-030-94583-1)